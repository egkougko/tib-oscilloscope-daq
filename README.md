# TIB Oscilloscope DAQ

## Getting started

This project contains the DAQ code to communicate with the oscilloscope SCPI interface in test beam conditions for data acquisition.
Several versions are available, for Tektronix and Agilent oscilloscopes in a single or dual scope implementation.

## Single Scope

Using an optocoupler with an MCP 2210, a single input is controlled. Once the input is set to high readout of the instrument commences and data acquisition resumes only when buffer is empty. 

## Bual Scope Mode

Tow versions of the code are available:

1. Using an optocoupler in combination with the MCP2210

As with the single scope version, only one input is monitored and triggers the instrument readout. The code works for Tektronix and AGILET oscilloscopes.

2. Use of the TiB (Trigger interface board)

Three TTL LEMO inputs are available for monitoring signals while five differential lines can set as inputs or outputs at the start of the acquisition for communicating with the TLU. Current implementation monitors two inputs and uses two of the differential lines for TLU synchronisation. A busy signal is set when the instrument is in readout mode, which is lowered once the oscilloscope returns in ready state. Instrument readout is trigger by one of the TTL LEMO inputs while the start of a new acquisition cycle is triggered by a secondary TTL LEMO input. 
Directionality of the differential lines is controlled by single D-type flip-flops, whose state is permanently set at the initialization of the DAQ program. 
