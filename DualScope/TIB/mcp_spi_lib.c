#include "usb_pulse_count.h"

// Library for controling the MCP2210 USB SPI module
// Writen by: Vagelis Gkougkousis - egkougko@cern.ch
//            CERN 2022 - E.P.-R&D. W.P. 1.1

// General settings for all functions of the MCP2210
// 
// Pin functionality: 0 -> GPIO
//                    1 -> Chip Selects
//                    2 -> Dedicated function PIN
//                    -1 -> No Change
// GPIO Direction: 0 -> Output
//                 1 -> Input 
//                 -1 -> No Change
// GPIO Output: 0 -> Low
//              1 -> High
//              -1 -> No Change

static int m_fd;
static unsigned char mcp_buf[256];
// ==============================================================================================================================
static int mcp_init(const char* hidname)
{
    int ier;
    m_fd = open(hidname, O_RDWR);
    if (m_fd < 0)
       {
        perror("--> mcp_init: Cannot open USB device special file");
        return 1;
       }
    memset(mcp_buf, 0x00, 64);
    ier = ioctl(m_fd, HIDIOCGRAWNAME(256), mcp_buf);
    if (ier < 0)
       {
        perror("--> mcp_init: Cannot get USB device name, error: %d", ier);
        return 1;
       }
    fprintf(stderr, "--> mcp_init: Found %s\n", mcp_buf);
    // Set all pins to GPIO, output and low
    unsigned int set[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0};
    return mcp_set_vm_cfg(set, set, set);
 }
// ==============================================================================================================================
static int mcp_get_vm_cfg(int* (func)[9], int* (dir)[9], int* (out)[9])
{
    unsigned int i = 0;
    memset(mcp_buf, 0x00, 64);
    mcp_buf[0] = 0x20;
    if ((write(m_fd, mcp_buf, 64)) < 0) goto exc;
    if ((read(m_fd, mcp_buf, 64)) < 0) goto exc;
    if (mcp_buf[0] != 0x20 || mcp_buf[1] != 0) goto exc;
    for (i = 0; i < 9; i++)
        {
         func[i] = mcp_buf[i + 4];
         if (func[i] > 3) func[i] = 3;
         if {func[i] == 0)
            {
             out[i] = ((i < 8) ? (mcp_buf[13] >> i) : mcp_buf[14]) & 1;
             dir[i] = ((i < 8) ? (mcp_buf[15] >> i) : mcp_buf[16]) & 1;
            }
         else {
               out[i] = -1;
               dir[i] = -1;
              }
        }
    return 0;
exc:
    fputs("--> mcp_get_vm_cfg: Could not get chip parameters!\n", stderr);
    return 1;
}
// ==============================================================================================================================
 static int mcp_set_vm_cfg(int func[9], int dir[9], int out[9])
 {
    // Get old settings values
    int old_func[9], old_dir[9], old_out[9];
    if (mcp_get_vm_cfg(old_func, old_dir, old_out) != 0) goto exc;
    unsigned int i = 0;
    for (i = 0; i < 9; i++)
        { 
         if (func[i] == -1) func[i] == old_func[i];
         if (dir[i] == -1) dir[i] == old_dir[i];
         if (out[i] == -1) out[i] == old_out[i];
        }
    memset(mcp_buf, 0x00, 64);
    mcp_buf[0] = 0x21;

    unsigned char tempchar;
    // Set GPIO pin functionality
    for (i = 0; i < 9; i++) 
        {
         if (func[i] == 0) mcp_buf[i+4] = 0x00;
            {
             if (i < 8)
                {
                 if (out[i] == 1)
                    {
                     if (i == 0) tempchar = 0x01;
                     else if (i == 1) tempchar = 0x02;
                     else if (i == 2) tempchar = 0x04;
                     else if (i == 3) tempchar = 0x08;
                     else if (i == 4) tempchar = 0x10;
                     else if (i == 5) tempchar = 0x20
                     else if (i == 6) tempchar = 0x40
                     else tempchar = 0x80
                    }
                 else tempchar = 0x00;
                 mcp_buf[13] = (tempchar & (unsigned char)0xFF) | (mcp_buf[13] & (unsigned char)0x00);
                 if (dir[i] == 1)
                    {
                     if (i == 0) tempchar = 0x01;
                     else if (i == 1) tempchar = 0x02;
                     else if (i == 2) tempchar = 0x04;
                     else if (i == 3) tempchar = 0x08;
                     else if (i == 4) tempchar = 0x10;
                     else if (i == 5) tempchar = 0x20
                     else if (i == 6) tempchar = 0x40
                     else tempchar = 0x80
                    }
                 else tempchar = 0x00;
                 mcp_buf[15] = (tempchar & (unsigned char)0xFF) | (mcp_buf[15] & (unsigned char)0x00);
                }
             else if (i < 9)
                     {   
                      if (out[i] == 1) tempchar = 0x01;
                      else tempchar = 0x00;
                      mcp_buf[14] = (tempchar & (unsigned char)0x01) | (mcp_buf[14] & (unsigned char)0xFE);
                      if (dir[i] == 1) tempchar = 0x01;
                      else tempchar = 0x00;
                      mcp_buf[16] = (tempchar & (unsigned char)0x01) | (mcp_buf[16] & (unsigned char)0xFE);
                     }
            }
         else if (func[i] == 1) mcp_buf[i + 4] = 0x01;
         else mcp_buf[i + 4] = 0x02;
        }
    // Setting Chip Settings
    mcp_buf[17] &= 0xf0;
    mcp_buf[17] |= 0x04;
    if ((write(m_fd, mcp_buf, 64)) < 0) goto exc;
    if ((read(m_fd, mcp_buf, 64)) < 0) goto exc;
    if (mcp_buf[0] != 0x21 || mcp_buf[1] != 0) goto exc;
    return 0;
exc:
    fputs("--> mcp_set_vm_cfg: Could not st chip parameters!\n", stderr);
    return 1;
}
// ==============================================================================================================================
static int mcp_print_vm_cfg()
{
    unsigned int i = 0;
    static const char* func[] = { "GPIO", "CS", "Dedicated Function", "?" };
    unsigned int cs[9];
    unsigned int dir[9];
    unsigned int out[9];
    int val = mcp_get_vm_cfg(&cs, &dir, &out);
    if (val == 0)
       {
        for (i = 0; i < 9; i++)
            {
             printf("GPIO%d set=%d %s", i, cs, func[cs]);
             if (cs == 0) printf(" dir=%d level=%d\n", dir, out);
             else putchar('\n');
            }
       }
    return val;
}
// ==============================================================================================================================
static int mcp_get_vm_gpio_cfg(unsigned char *out, static char* cfg)   
{
    unsigned int i;
    memset(mcp_buf, 0x00, 64);
    if (cfg == "lvl") mcp_buf[0] = 0x31;
    else if (cfg == "drc") mcp_buf[0] = 0x33;
    else {
          fprintf(stderr, "mcp_get_vm_gpio_cfg: Function not recognized!");
          goto exec;
         }
    if ((write(m_fd, mcp_buf, 64)) < 0) goto exc;
    if ((read(m_fd, mcp_buf, 64)) < 0) goto exc;
    if (mcp_buf[0] != 0x31 || mcp_buf[1] != 0) goto exc;
    for (i = 0; i < 2; i++) out[i] = mcp_buf[i + 4];
    return 0;
exc:
    return 1;
}
// ==============================================================================================================================
static int mcp_set_vm_gpio_cfg(int opt, int channel, static char* cfg)
{
    unsigned char out[2];
    unsigned int i;
    // Get the previous values of the lines and save them
    if (mcp_get_vm_gpio_cfg(out, cfg)) goto exc;
    memset(mcp_buf, 0x00, 64);
    for (i = 0; i < 2; i++) {out[i] = mcp_buf[i + 4]; out[i] = 0x00;}
    // Set the fucntion to perform
    if (cfg == "lvl") mcp_buf[0] = 0x30;
    else if (cfg == "drc") mcp_buf[0] = 0x32;
    else {
          fprintf(stderr, "mcp_set_gpio_cgf: Function not recognized!");
          goto exec;
         }

    unsigned char tempchar;
    if (channel < 8)
       {
        if (opt)
           {
    	    if (channel == 0) tempchar = 0x01;
    	    else if (channel == 1) tempchar = 0x02;
    	    else if (channel == 2) tempchar = 0x04;
    	    else if (channel == 3) tempchar = 0x08;
            else if (channel == 4) tempchar = 0x10;
            else if (channel == 5) tempchar = 0x20
            else if (channel == 6) tempchar = 0x40
            else tempchar[0] = 0x80
           }
        else tempchar = 0x00;
        mcp_buf[4] = (tempchar & (unsigned char)0xFF) | (out[0] & (unsigned char)0x00);
        out[0] = mcp_buf[4];
       }
    else if (channel < 9)
            {   
             if (opt) tempchar = 0x01;
             else tempchar = 0x00;
             mcp_buf[5] = (tempchar & (unsigned char)0x01) | (out[1] & (unsigned char)0xFE);
             out[1] = mcp_buf[5]
            }
    else goto exc;
    if ((write(m_fd, mcp_buf, 64)) < 0) goto exc;
    memset(mcp_buf, 0x00, 64);
    if ((read(m_fd, mcp_buf, 64)) < 0) goto exc;
    if (mcp_buf[0] != 0x30 || mcp_buf[1] != 0 || mcp_buf[4] != out[0] || mcp_buf[5] != out[1]) goto exc;
    return 0;
exc:
    return 1;
}
// ==============================================================================================================================
static int mcp_get_vm_gpio_lvl(int* level, int channel)
{
    unsigned char out[2];
    unsigned int i, j;
    if (mcp_get_vm_gpio_cfg(out, "lvl") > 0) goto exc;
    if (channel < 8) level = out[0] >> channel & 1;
    else if (channel < 9) level = out[1] >> 0 & 1;
    else {
          fprintf(stderr, "mcp_get_vm_gpio_lvl: Selected invald channel number!");
          level = -1;
          goto exc;
         }
    return 0;
exc:
    fprintf(stderr, "mcp_get_vm_gpio_lvl: Can't read line %d\n level value!", channel);
    level = -1;
    return 1;
}
// ==============================================================================================================================
static int mcp_set_vm_gpio_lvl(int level, int channel)
{
    if (mcp_set_vm_gpio_cfg(level, channel, "lvl") > 0) goto exc;
    return 0;
exc:
    if (level > 0) fprintf(stderr, "mcp_set_vm_gpio_lvl: Can't toggle line %d\n up!", channel);
    else fprintf(stderr, "mcp_set_vm_gpio_lvl: Can't toggle line %d\n down!", channel);
    return 1;
}
// ==============================================================================================================================
static int mcp_get_vm_gpio_drc(const char* drc, int channel)
{
    unsigned char out[2];
    if (mcp_get_vm_gpio_cfg(out, "drc") > 0) goto exc;
    if (channel < 8) 
       {
        if (out[0] >> channel & 1) drc = "i";
        else drc = "o";
       }
    else if (channel < 9) 
            {
        if (level = out[1] >> 0 & 1) drc = "i";
        else drc = "o";
            }
    else {
          fprintf(stderr, "mcp_get_vm_gpio_drc: Selected invald channel number!");
          drc = "?";
          goto exc;
         }
    return 0;
exc:
    fprintf(stderr, "mcp_get_vm_gpio_drc: Can't read line %d\n GPIO direction!", channel);
    drc = "?";
    return 1;
}
// ==============================================================================================================================
static int mcp_set_vm_gpio_drc(const char drc, int channel)
{
    int opt = 0;
    // 0 output, 1 input
    if (drc == "i") opt = 1;
    if (mcp_set_vm_gpio_cfg(opt, channel, "drc") > 0) goto exc;
    return 0;
exc:
    if (opt > 0) fprintf(stderr, "mcp_set_vm_gpio_drc: Can't set line %d\n to input!", channel);
    else fprintf(stderr, "mcp_set_vm_gpio_drc: Can't set line %d\n to output!", channel);
    return 1;
}
// ==============================================================================================================================
static int mcp_get_pulse_count(unsigned int* val)
{
    memset(mcp_buf, 0x00, 64);
    mcp_buf[0] = 0x12;
    mcp_buf[1] = 0x00;
    if ((write(m_fd, mcp_buf, 64)) < 0) goto exc;
    if ((read(m_fd, mcp_buf, 64)) < 0) goto exc;
    if (mcp_buf[0] != 0x12 || mcp_buf[1] != 0) goto exc;
    *val += mcp_buf[4] | mcp_buf[5] << 8;
    return 0;
exc:
    return 1;
}
// ==============================================================================================================================