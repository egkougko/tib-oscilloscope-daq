#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>

// Library for controling the MCP2210 USB SPI module
// Writen by: Vagelis Gkougkousis - egkougko@cern.ch
//            CERN 2022 - E.P.-R&D. W.P. 1.1

static int mcp_init(const char* hidname);
static int mcp_get_vm_cfg(unsigned int* (func)[9], unsigned int* (dir)[9], unsigned int* (out)[9]);
static int mcp_set_vm_cfg(unsigned int func[9], unsigned int dir[9], unsigned int out[9]);
static int mcp_print_vm_cfg();
static int mcp_get_vm_gpio_cfg(unsigned char* out, static char* cfg);
static int mcp_set_vm_gpio_cfg(int opt, int channel, static char* cfg);
static int mcp_get_vm_gpio_lvl(int* level, int channel);
static int mcp_get_vm_gpio_drc(const char* drc, int channel);
static int mcp_set_vm_gpio_lvl(int level, int channel);
static int mcp_set_vm_gpio_drc(const char drc, int channel);
static int mcp_get_pulse_count(unsigned int* val);