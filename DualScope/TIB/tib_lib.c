#include "usb_pulse_count.h"

// Library for programming the EUDAT Trigger Interface board V2.0
// base on an MCP 2210 SPI to USB microcontroller
// Writen by: Vagelis Gkougkousis - egkougko@cern.ch
//            CERN 2022 - E.P.-R&D. W.P. 1.1
 
// GPIO Lines Connectivity table: GPIO0 -> LVDS line 2
//                                GPIO1 -> LVDS line 4
//                                GPIO2 -> LVDS line 1 
//                                GPIO3 -> LVDS line 0
//                                GPIO4 -> LVDS line 3
//                                GPIO6 -> Multiplexer In

// Multiplexer select lines connectivity: S0 -> GPIO5 
//                                        S1 -> GPIO7
//                                        S2 -> GPIO8

// Multiplexer connections: Output0 -> X3 LEMO
//                          Output1 -> X2 LEMO
//                          Output2 -> X1 LEMO
//                          Output3 -> LVDS Line 2
//                          Output4 -> LVDS Line 4
//                          Output5 -> LVDS Line 1
//                          Output6 -> LVDS Line 0
//                          Output7 -> LVDS Line 3

//  LVDS Lines Correspondance: Line 0 -> Clock
//                             Line 1 -> Busy
//                             Line 2 -> Trigger
//                             Line 3 -> Spare
//                             Line 4 -> Count

static int tib_selct_mux_line(int outp);
static int tib_set_diff_dir(const char dir, int line);

// ==============================================================================================================================
//Selection between multiplexer outputs
static int tib_selct_mux_line(int outp)
{
    if (outp > 7 || line < 0)
       {
        fprintf(stderr, "Invalid differential line number: %d!\n", line);
        goto exc;
       }
    // 2. Set correct NVRAM settings
    int func[9] = { -1, -1, -1, -1, -1, 0, -1, 0, 0 }; // set pins 5, 7, 8 as GPIO
    int dir[9] = { -1, -1, -1, -1, -1, 0, -1, 0, 0 }; // set direction of pins 5, 7, 8 as outputs
    int out[9] = { -1, -1, -1, -1, -1, 0, 1, 0, 0 };  // initialize the value of pins 5, 7, 8 to zero
    if (outp == 1) { out[5] = 1; out[7] = 0; out[8] = 0; } // A1
    else if (outp == 2) { out[5] = 0; out[7] = 1; out[8] = 0; } // A2
    else if (outp == 3) { out[5] = 1; out[7] = 1; out[8] = 0; } // A3
    else if (outp == 4) { out[5] = 0; out[7] = 0; out[8] = 1; } // A4
    else if (outp == 5) { out[5] = 1; out[7] = 0; out[8] = 1; } // A5
    else if (outp == 6) { out[5] = 0; out[7] = 1; out[8] = 1; } // A6
    else if (outp == 7) { out[5] = 1; out[7] = 1; out[8] = 1; } // A7
    if (mcp_set_vm_cfg(func, dir, out) goto exc;
    return 0;
 exc:
    fprintf(stderr, "tib_selct_mux_line: Error selecting multiplexer output %d\n", outp);
    return 1;
}
// ==============================================================================================================================
static int tib_set_diff_dir(const char dir, int line)
{
    if (line > 4 || line < 0)
       {
        fprintf(stderr, "Invalid differential line number: %d!\n", line);
        goto exc;
       }
    // 1. Decoding the correct output for the multiplexer and the latches
    int mxline = 0;
    int mcline = 0;
    if (line == 2) { mxline = 3; mcline = 0; }
    else if (line == 4) { mxline = 4; mcline = 1; }
    else if (line == 1) { mxline = 5; mcline = 2; }
    else if (line == 0) { mxline = 6; mcline = 3; }
    else { mxline = 7; mcline = 4; }
    // 2. Get old NVRAM settings and save them
    int old_func[9], old_dir[9], old_out[9];
    if (mcp_get_vm_cfg(old_func, old_dir, old_out) != 0) goto exc;
    // 3. Set Special function of PINs 0 - 4 and 6 as generic GPIO with directionality as output
    // and pin 6 to high to enabal latch to accept progtamming
    int func[9] = {0, 0, 0, 0, 0, -1, 0, -1, -1};
    int dir[9] = {0, 0, 0, 0, 0, -1, 0, -1, -1};
    int out[9] = {-1, -1, -1, -1, -1, -1, 1, -1, -1}; 
    if (mcp_set_vm_cfg(func, dir, out) goto exc;
    // 4. Connect pin 6 to the apropritate latch channel via the multiplexer funtion
    tib_selct_mux_line(mxline);    
    // 5. Set the GPIO pin that is cpmnnected to the selected latch to high or low with respect to if we want it to be input or output
    if (dir == "t") if (mcp_set_vm_gpio_lvl(1, mcline)) goto exc; // High is in transmit mode
    else if (mcp_set_vm_gpio_lvl(0, mcline)) goto exc; // low is in receive mode    
    // 6. set pin 6 to low indicating programming finished
    if (mcp_set_vm_gpio_lvl(0, 6)) goto exc;
    // 7. Restore old nv ram settings
    if (mcp_set_vm_cfg(old_func, old_dir, old_out) goto exc;
    return 0;
exc:
    fprintf(stderr, "tib_set_diff_dir: Error setting differential line direction %d\n", line);
    return 1;
}
// ==============================================================================================================================
int test_sps(int argc, char** argv)
{
    int ier;
    int i;
    int j;
    unsigned int early_warning = 0;
    unsigned int spil_start = 0;
    unsigned int spil_end = 0;
    unsigned int cnt = 0;
    char line[20];

    if (mcp_init("/dev/hidraw2")) return 1;
    usleep(10000);
    if (mcp_print_vm_cfg) return 1;
    int func[9] = { -1, -1, -1, -1, -1, -1, 2, -1, -1 };
    int dir[9] = { -1, -1, -1, -1, -1, -1, 1, -1, -1 };
    int out[9] = { -1, -1, -1, -1, -1, -1, 1, -1, -1 };
    if (mcp_set_vm_cfg(func, dir, out) return 1;

    ier = 0;
    for (i = 0; i < 100; i++)
       {
        for (j = 0; j < 3; j++)
            {
             if (tib_selct_mux_line(j))
                {
                 fprintf(stderr, "ERROR while trying to select line %u at cycle %u\n", j, i);
                 ier = 1;
                 break;
                }
             memset(line, '\0', sizeof(line));
             if (j == 0) { cnt = early_warning; strncpy(line, "early warning", 13); }
             else if (j == 1) { cnt = spil_start; strncpy(line, "start of spill", 12); }
             else { cnt = spil_end; strncpy(line, "end of spill", 14); }
             fprintf(stderr, "--> Probing %s at cycle %u with previously %u events\n", line, i, cnt);
             if (mcp_print_vm_cfg) return 1;
             cnt = 0;
             do {
                 if (mcp_get_pulse_count(&cnt))
                    {
                     fprintf(stderr, "ERROR while reading pulse count in cycle: %u\n", i);
                     ier = 1; break;
                    }
                 else {
                       printf("%d\n", cnt);
                       usleep(500000);
                      }
                } while (cnt == 0);
             if (ier = 1) break;
             if (j == 0) early_warning += cnt;
             else if (j == 1) spil_start += cnt;
             else spil_end += cnt;
            }
        if (ier = 1) break;
       }
    return 0;
}
